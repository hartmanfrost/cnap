﻿#Requires -Version 5.0

using namespace System.IO;
<#
### Elevate Credentials ###
param([switch]$Elevated)
function Check-Admin {
    $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
    $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator) }
    if ((Check-Admin) -eq $false)  { 
        if ($elevated){ # Could not elevate, quit
    } else { 
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -WindowStyle Hidden -file "{0}" -elevated' -f ( $myinvocation.MyCommand.Definition ))
    } exit
}
#>

[Program]::main();

Class Program
{
    # const
    static [string] $hashUrl = "";

    static [Void] main() {

        return;
    }

    static [Void] checkUpdate() {
        
    }

}