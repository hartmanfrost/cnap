﻿#Requires -Version 5.0

using namespace System.IO;
using namespace System.IO.Compression;
using namespace System.Net;
using namespace System.Collections.Generic;
using namespace System.Linq;
using namespace System.Security.Cryptography;



<#
### Elevate Credentials ###
param([switch]$Elevated)
function Check-Admin {
    $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
    $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator) }
    if ((Check-Admin) -eq $false)  { 
        if ($elevated){ # Could not elevate, quit
    } else { 
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -WindowStyle Hidden -file "{0}" -elevated' -f ( $myinvocation.MyCommand.Definition ))
    } exit
}
#>

function unzip {
	param( [string]$ziparchive, [string]$extractpath )
	[System.IO.Compression.ZipFile]::ExtractToDirectory( $ziparchive, $extractpath )
}



[Program]::main();

Class Program
{
    # const
    static [string]$url = "https://bitbucket.org/hartmanfrost/cnap/get/master.zip";
    
    # param

    #main
    static [void] main() {
        # Remember initial list of files
        [List[string]]$Lfile0 = [List[string]]::new();
        [List[string]]$Lhash0 = [List[string]]::new();
        [Program]::GetFilesWithHashes([ref] $Lfile0, [ref] $Lhash0);

        # Download repo
        [Program]::DownloadRepo();

        # Remember new list
        [List[string]]$Lfile1 = [List[string]]::new();
        [List[string]]$Lhash1 = [List[string]]::new();
        [Program]::GetFilesWithHashes([ref] $Lfile1, [ref] $Lhash1);

    }

    static [void] DownloadRepo(){
        [string] $pathArchive = [Path]::Combine($PSScriptRoot, "master.zip");
        ([WebClient]::new()).DownloadFile([Program]::url, $pathArchive);
        #[System.IO.Compression.ZipFile]::ExtractToDirectory($pathArchive, [Path]::GetPathRoot());
    }

    static [void] GetFilesWithHashes([ref] $Lfile0, [ref] $Lhash0) {
        # Get list files
        $Lfile0.Value.AddRange([Directory]::GetFiles($PSScriptRoot));
        $Lfile0.Value = [Enumerable]::ToList([Enumerable]::Where(
            $Lfile0.Value,
            [Func[string,bool]]{param($f); return !$f.EndsWith("hash.txt"); }
        ));
        For([int]$i = 0; $i -lt $Lfile0.Value.Count; $i++) {
            $Lhash0.Value.Add([Program]::CalcHash($Lfile0.Value[$i]));
        }
    }

    static [string] CalcHash([String]$file) {
        [SHA512] $sha512Hash = [SHA512]::Create();
        [FileStream] $stream = [File]::OpenRead($file);
        
        return [System.BitConverter]::ToString($sha512Hash.ComputeHash($stream)).Replace("-", [String]::Empty).ToLowerInvariant();
    }
}

<#
Class Program
{
    # const
    
    # param

    #main
    static [Void] main() {
        # Get list files
        $Lfiles = [List[string]]::new();
        $Lfiles.AddRange([Directory]::GetFiles($PSScriptRoot));
        $Lfiles = [Enumerable]::ToList([Enumerable]::Where(
            $Lfiles,
            [Func[string,bool]]{param($f); return !$f.EndsWith("hash.txt"); }
        ));
        # Open writer
        [StreamWriter]$file = [StreamWriter]::new([Path]::Combine($PSScriptRoot, "hash.txt"));
        For([int]$i = 0; $i -lt $Lfiles.Count; $i++) {
            $file.WriteLine([Path]::GetFileName($Lfiles[$i]) + " " + [Program]::CalcHash($Lfiles[$i]));
        }
        # Close writer
        $file.Close();
    }

    static [String] CalcHash([String]$file) {
        [SHA512] $sha512Hash = [SHA512]::Create();
        [FileStream] $stream = [File]::OpenRead($file);
        
        return [System.BitConverter]::ToString($sha512Hash.ComputeHash($stream)).Replace("-", [String]::Empty).ToLowerInvariant();
    }
}
#>