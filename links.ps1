﻿#Requires -Version 5.0

using namespace System.IO;

### Elevate Credentials ###
param([switch]$Elevated)
function Check-Admin {
    $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
    $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator) }
    if ((Check-Admin) -eq $false)  { 
        if ($elevated){ # Could not elevate, quit
    } else { 
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -WindowStyle Hidden -file "{0}" -elevated' -f ( $myinvocation.MyCommand.Definition ))
    } exit
}

function Cleanup() {
    
}

function CreateLinks() {
    $shell = New-Object -comObject Wscript.Shell;
    For([int]$i = 0; $i -lt $Lpath.Count; $i++) {
        if(Test-Path $Lpath[$i].Item1) {
            [string]$pathToLink = [Path]::Combine([Environment]::GetFolderPath("CommonDesktopDirectory"), $Lpath[$i].Item3) + ".lnk";
            if(Test-Path $pathToLink) {
                Remove-Item $pathToLink;
            }
            
            $shortcut = $shell.CreateShortcut($pathToLink);
            $shortcut.TargetPath = $Lpath[$i].Item1;
            $shortcut.WorkingDirectory = [Path]::GetDirectoryName($Lpath[$i].Item1);
            if(!([String]::IsNullOrEmpty($Lpath[$i].Item2))) {
                $shortcut.Arguments = $Lpath[$i].Item2;
            }
            if(!([String]::IsNullOrEmpty($Lpath[$i].Item4))) {
                [string]$pathToFavicon = "";
                DownloadFavicon $Lpath[$i].Item4 $Lpath[$i].Item3 ([ref]$pathToFavicon);
                #$pathToFavicon = [string]$pathToFavicon;
                if(!([String]::IsNullOrEmpty($pathToFavicon))) {
                    $shortcut.IconLocation = $pathToFavicon;
                }
            }
            $shortcut.Description = $Lpath[$i].Item3;
            $shortcut.Save();
        }
    }
}

function DownloadFavicon([string]$url, [string]$name, [ref]$fullPath) {
    [string]$systemDrive = [Path]::GetPathRoot([Environment]::SystemDirectory);
    [string]$baseDir = "favicons";
    [string]$path = [Path]::Combine($systemDrive, $baseDir, $name);
    $fullPath.Value = [Path]::Combine($path, "favicon.ico");
    if(Test-Path $fullPath.Value) {
        return;
    }
    if(!(Test-Path $path)) {
        New-Item $path -ItemType Directory;
    }
    (New-Object System.Net.WebClient).DownloadFile($url, $fullPath.Value);
    return;
}

$Lpath = New-Object System.Collections.ArrayList;
$Lpath.AddRange((
    [Tuple]::Create("C:\Program Files\Qlogic\ClerkWork\ClerkWork.exe", "", "Електронна черга", ""),
    [Tuple]::Create("C:\Program Files (x86)\IRC\EDR_EDS\Client.exe", "", "ЄДР юридичних осіб та фізичних осіб-підприємців", ""),
    [Tuple]::Create("C:\Program Files (x86)\Google\Chrome\Application\chrome.exe", "erc1.citynet.kharkov.ua", "Дозвільний центр", "http://erc1.citynet.kharkov.ua/_layouts/images/eTownImages/favicon_kharkiv.ico"),
    [Tuple]::Create("C:\Program Files (x86)\Google\Chrome\Application\chrome.exe", "portal.nazk.gov.ua", "ЄДР Декларацій", "https://portal.nazk.gov.ua/favicon.ico"),
    [Tuple]::Create("C:\Program Files (x86)\Google\Chrome\Application\chrome.exe", "register.minjust.gov.ua", "ЄДР Нерухомого Майна", "https://register.minjust.gov.ua/images/UBLogo16.ico"),
    [Tuple]::Create("C:\Program Files\Mozilla Firefox\firefox.exe", "https://e-kharkiv.org", "Портал електронних сервісів міста Харкова", "https://e-kharkiv.org/static/img/favicon/favicon.ico"),
    [Tuple]::Create("C:\Program Files\internet explorer\iexplore.exe", "http://srv-fe-01.csp.local:5555", "CRM Прозорий офіс", ""),
    [Tuple]::Create("D:\Как пользоваться новой обменкой.docx", "", "Как пользоваться обменкой", "")
));

Cleanup;
CreateLinks;